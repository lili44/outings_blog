function onSelectSearch() {
    let search = $('#search option:selected');

    $.get(search.data('url'), function(data) {
        if (data!== undefined) {
            $('#search-posts').html(data);
        }
    });
}
