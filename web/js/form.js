let photoCount = '{{ formPost.photos|length }}';

$(document).ready(function() {
    $('#add-another-email').on('click', function(event) {
        event.preventDefault();
        let photoList = $('#photo-fields-list');

        let newWidget = photoList.attr('data-prototype');
        newWidget = newWidget.replace(/__name__/g, photoCount);
        photoCount++;

        let newLi = $('<li></li>').html(newWidget);
        newli.appendTo(photoList);
    });
});
