let FormValidator = function (form) {
    this.form    = form;
    this.totalError;
};

FormValidator.prototype.checkPassword = function () {
    console.log('checkPassword');
    let error = 0;
    this.form.find('[type="password"]').each(function() {
        let msgError = {
            length : "Votre mot de passe doit contenir au moins 8 caractères.",
            regex  : "Votre mot de passe doit contenir au moins 1 lettre, 1 chiffre et 1 caractère spécial (parmi &, #, $, {, }, @, *, -, !, %)."
        };
        let listError = $('<ul>').addClass('errorList');
        let value = $(this).val().trim();
        if (value.length < 8) {
            error++;
            $('<li>')
                .addClass('error')
                .text(msgError.length)
                .appendTo(listError);
        }
        if (value.search(/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z{}&!@#$%*\-]{8,}$/) == -1) {
            error++;
            $('<li>')
                .addClass('error')
                .text(msgError.regex)
                .appendTo(listError);
        }
        if (error > 0) {
            $(this).addClass('errorField');
            listError.insertAfter($(this));
        }
    });
    this.totalError += error;
    console.log(`totalError : ${this.totalError}`);
}

FormValidator.prototype.checkLength = function () {
    console.log('checkLength');
    let error = 0;
    this.form.find('[data-length]').each(function() {
        let value = $(this).val().trim();
        let maxLength = $(this).data('length');
        if (value.length > maxLength) {
            error++;
            $(this).addClass('errorField');
            $('<p>')
                .addClass('error')
                .text(`Le champ doit contenir au maximum ${maxLength} caractères.`)
                .insertAfter($(this));
        }
    });
    this.totalError += error;
    console.log(`totalError : ${this.totalError}`);
}

FormValidator.prototype.checkEmail = function () {
    console.log('checkEmail');
    let error = 0;
    this.form.find('[type="email"]').each(function() {
        let value = $(this).val().trim();
        if (value.search(/^[a-z0-9._-]+@[a-z]+\.[a-z]{2,4}$/) == -1) {
            error++;
            $(this).addClass('errorField');
            $('<p>')
                .addClass('error')
                .text("Adresse invalide.")
                .insertAfter($(this));
        }
    });
    this.totalError += error;
    console.log(`totalError : ${this.totalError}`);
}

FormValidator.prototype.checkRequired = function () {
    console.log('checkRequired');
    let error = 0;
    this.form.find('input[required]:not([type="submit"], textarea[required])').each(function() {
        let value = $(this).val().trim();
        if (value.length == 0) {
            error++;
            $(this).addClass('errorField');
            $('<p>')
                .addClass('error')
                .text("Le champ est obligatoire.")
                .insertAfter($(this));
        }
    });
    this.totalError += error;
    console.log(`totalError : ${this.totalError}`);
}

FormValidator.prototype.checkConfirmPass = function () {
    console.log('checkConfirmPass');
    let error = 0;
    if (this.form.find('[data-confirm]').length > 0) {
        let passOne = this.form.find('[data-confirm = "1"]');
        let passTwo = this.form.find('[data-confirm = "2"]');
        if (passOne.val().trim() !== passTwo.val().trim()) {
            error++;
            $(this).addClass('errorField');
            $('<p>')
            .addClass('error')
            .text("La confirmation doit être identique au mot de passe.")
            .insertAfter(passTwo);
        }
    }
    this.totalError += error;
    console.log(`totalError : ${this.totalError}`);
}

FormValidator.prototype.onSubmit = function (event) {
    console.log('onSubmit');
    this.form.find('.error').each(function() {
        $(this).empty();
    });
    this.totalError = 0;
    this.checkRequired();
    this.checkLength();
    this.checkEmail();
    this.checkPassword();
    this.checkConfirmPass();

    if (this.totalError > 0) {
        this.form.find('.error').fadeIn('slow');
        event.preventDefault();
    }
}

FormValidator.prototype.run = function() {
    this.form.on('submit', this.onSubmit.bind(this));
    console.log(`Erreur : ${this.totalError}`);
}
