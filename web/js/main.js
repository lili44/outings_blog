$(function() {
    // Affichage des message Flash
    $(('.alert')).delay(3000).fadeOut('slow');

    // Validation des formulaires
    let form = $('form[data-validation=true]');
    console.log(`form : ${form.length}`);
    if (form.length > 0) {
        let formValidator = new FormValidator(form);
        formValidator.run();
    }

    let search = $('#search');
    if (search.length > 0) {
        $('#search').on('change', onSelectSearch);
    }
});
