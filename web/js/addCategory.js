let $collectionHolder;
// Création d'un bouton d'ajout de catégorie
let $addTagLink = $('<a href="#" class="add_tag_link">Ajouter une catégorie</a>');
let $newLinkLi = $('<li></li>').append($addTagLink);

function addCategoryForm ($collectionHolder, $newLinkLi) {
    let prototype = $collectionHolder.data('prototype');
    let index = $collectionHolder.data('index');

    let newForm = prototype;
    newForm = newForm.replace(/__name__/g, index);

    $collectionHolder.data('index', index + 1);

    let $newFormLi = $('<li></li>').append(newForm);
    $newLinkLi.before($newFormLi);
}

$(document).ready(function() {
    // Récupération de la liste
    $collectionHolder = $('ul.categories');

    // Ajout du bouton
    $collectionHolder.append($newLinkLi);

    // Décompte du nombre de champ déjà ajouté
    $collectionHolder.data('index', $collectionHolder.find(':input').length);

    // Gestionnaire d'évènement au clic sur le bouton
    $addTagLink.on('click', function(e) {
        // Annulation de l'envoi du formulaire
        e.preventDefault();
        // Ajout d'un champ pour ajouter une catégorie
        addCategoryForm($collectionHolder, $newLinkLi);
    });
});
