L'Ecole prend l'air
========================

Code source d'un site de démonstration développé avec le framework Symfony 3 - Standard Edition (Copyright (c) 2004-2017 Fabien Potencier).

Installation
------------

### 1. Récupérer le code

Télécharger le code source [ici][1]

### 2. Définir vos paramètres

Renommer le fichier `app/config/parameters.yml.dist` en `parameters.yml` et modifier les valeurs des paramètres.

### 3. Télécharger les vendors

Avec Composer :

    php composer.phar install

### 4. Créer la base de données

* Création de la base de données :

        php bin/console doctrine:database:create

* Création des tables :

        php bin/console doctrine:schema:update --dump-sql
        php bin/console doctrine:schema:update --force

* Vous pouvez la remplir avec des fixtures :

        php bin/console doctrine:fixtures:load

Auteur
------

* Elise Borie


[1]: https://bitbucket.org/lili44/outings_blog/get/642b7bafb828.zip
