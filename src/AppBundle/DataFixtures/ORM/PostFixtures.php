<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Post;
use AppBundle\Entity\Comment;
use AppBundle\DataFixtures\FixturesTrait;
use AppBundle\DataFixtures\ORM\CategoryFixtures;
use AppBundle\DataFixtures\ORM\UserFixtures;
use AppBundle\DataFixtures\ORM\PhotoFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PostFixtures extends Fixture
{
    use FixturesTrait;

    public function load(ObjectManager $manager)
    {
        foreach ($this->getRandomPostTitles() as $i => $title) {
            $post = new Post();
            $post->setCreatedAt(new \DateTime('now - ' . $i . 'days'));
            $post->setTitle($title);
            $post->setContent($this->getRandomPostContent());
            $post->setIsPublished($this->getRandomIsPublished());

            if ($post->getIsPublished() == true) {
                $post->setPublishedAt($post->getCreatedAt());

                foreach (range(0, mt_rand(0, 5)) as $index) {
                    $comment = new Comment();
                    $comment->setCreatedAt(new \DateTime('now + ' . $index . 'seconds'));
                    $comment->setContent($this->getRandomCommentContent());
                    $randomNum = mt_rand(0, 1);
                    if ($randomNum === 1) {
                        $comment->setAuthor($this->getRandomUser());
                    }
                    $post->addComment($comment);

                    $manager->persist($comment);
                }
            }

            foreach($this->getRandomCategories(mt_rand(1, 4)) as $category) {
                $post->addCategory($category);
            }

            $post->setAuthor($this->getRandomUser());
            $post->setPhoto($this->getReference("photo-$i"));

            $manager->persist($post);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            CategoryFixtures::class,
            UserFixtures::class,
            PhotoFixtures::class,
        );
    }

    public function getRandomCategories($number = 1)
    {
        $categories = [];
        $indexes = (array) array_rand($this->getCategoryNames(), $number);
        foreach ($indexes as $i) {
            $categories[] = $this->getReference('category-'.$i);
        }
        return $categories;
    }

    public function getRandomUser()
    {
        $users = $this->getUsernames();
        $index = array_rand($users);
        return $this->getReference('user-'.$index);
    }

    public function getRandomIsPublished()
    {
        $isPublished = [true, false];
        return $isPublished[array_rand($isPublished)];
    }
}
