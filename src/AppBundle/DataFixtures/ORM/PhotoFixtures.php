<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Photo;
use AppBundle\DataFixtures\FixturesTrait;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PhotoFixtures extends Fixture
{
    use FixturesTrait;

    public function load(ObjectManager $manager)
    {
        $titles = $this->getPhotoTitles();

        foreach ($titles as $i => $title) {
            $photo = new Photo();
            $photo->setTitle($title);
            $photo->setUrl("uploads/sample$i.jpg" );
            $manager->persist($photo);
            $this->addReference("photo-$i", $photo);
        }

        $manager->flush();
    }
}
