<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use AppBundle\DataFixtures\FixturesTrait;
use AppBundle\DataFixtures\ORM\AvatarFixtures;
USE Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    use FixturesTrait;

    public function load(ObjectManager $manager)
    {
        $encoder = $this->container->get('security.password_encoder');
        // Création des utilisateurs
        $listUsernames = $this->getUsernames();
        foreach($listUsernames as $i => $username) {
            $user = new User();
            $user->setUsername($username);
            $user->setFirstname('F'.$username);
            $user->setLastname('L'.$username);
            $user->setEmail(strtolower($username).'@gmail.com');
            $plainPass = strtolower($username).'pass1*';
            $encodedPass = $encoder->encodePassword($user, $plainPass);
            $user->setPassword($encodedPass);
            $user->setIsActive(true);
            $user->setAvatar($this->getRandomAvatar());
            $manager->persist($user);
            $this->addReference("user-$i", $user);
        }
        // Création de l'admin
        $admin = new User();
        $admin->setUsername('admin');
        $admin->setFirstname('Fadmin');
        $admin->setLastname('Ladmin');
        $admin->setEmail('admin@gmail.com');
        $encodedPass = $encoder->encodePassword($admin, 'adminpass1*');
        $admin->setPassword($encodedPass);
        $admin->setRoles(array('ROLE_SUPER_ADMIN'));
        $admin->setIsActive(true);
        $admin->setAvatar($this->getReference('avatar-0'));
        $manager->persist($admin);
        $this->addReference('admin', $admin);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(AvatarFixtures::class);
    }

    public function getRandomAvatar()
    {
        $index = array_rand($this->getAvatarNames());
        return $this->getReference('avatar-' . $index);
    }
}
