<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Avatar;
use AppBundle\DataFixtures\FixturesTrait;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AvatarFixtures extends Fixture
{
    use FixturesTrait;

    public function load(ObjectManager $manager)
    {
        $names = $this->getAvatarNames();

        foreach ($names as $i => $name) {
            $avatar = new Avatar();
            $avatar->setName($name);
            $avatar->setUrl("img/avatar/avatar$i.png");
            $manager->persist($avatar);
            $this->addReference('avatar-'.$i, $avatar);
        }
        $manager->flush();
    }
}
