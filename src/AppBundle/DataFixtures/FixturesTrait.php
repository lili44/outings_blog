<?php
namespace AppBundle\DataFixtures;

/**
*
*/
trait FixturesTrait
{
    function getPostContents()
    {
        return [
            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta quod maiores a culpa ab doloribus neque quo quos molestiae magnam repudiandae recusandae repellendus voluptatum vel quas temporibus alias nobis, corporis nesciunt velit fugit! Magnam ea, cumque deleniti? Aliquid maiores est quas similique ad! Commodi illum sint in ratione ipsa necessitatibus accusantium maiores amet quisquam quae hic neque nihil nulla quaerat sequi odio nemo at labore, dolor aut et saepe voluptatem numquam sit. Debitis corrupti itaque, odit qui quae adipisci at ex quibusdam facere maiores culpa ut et vitae! Reiciendis temporibus nihil, doloremque odio natus? Doloribus earum repellendus illum neque eaque?',
            'Fugit quasi iste minima cumque laborum est laudantium provident ad perspiciatis culpa consequuntur ratione nobis vero illum reprehenderit, quos labore suscipit excepturi illo, nam quibusdam ducimus rerum. Amet impedit veritatis quod iure nobis velit aspernatur! Possimus, libero voluptatem! Obcaecati veniam iste minima esse non dignissimos sequi alias quae, itaque voluptatum totam quia eius dolorem voluptatibus ipsum quibusdam quas, suscipit! Nulla, animi suscipit ad eveniet voluptates, modi laudantium quisquam necessitatibus alias odio nemo reprehenderit debitis, ipsa reiciendis. Ipsa, ipsam tempora maiores, animi esse et perspiciatis excepturi praesentium corporis placeat aliquam blanditiis iste adipisci dignissimos. Ducimus rem sequi ea, soluta assumenda quo.',
            'Laudantium, saepe, similique? Labore, consequatur, doloribus! Quis labore totam, adipisci aliquam repudiandae quisquam. Neque praesentium qui nesciunt consectetur consequatur dolor suscipit minus ullam molestiae quisquam optio, fuga explicabo exercitationem numquam incidunt. Sunt aliquid officia blanditiis aut eligendi consequuntur, asperiores, laudantium quisquam dolor laborum atque. Labore libero placeat magnam accusamus eligendi esse tenetur voluptatem ratione eius. Inventore dolores, nemo illum cum libero numquam eaque enim, tempore fuga in consequatur repellendus quas iusto consequuntur corporis soluta. Facere doloremque ex tempore nesciunt assumenda, molestiae, voluptatibus voluptates excepturi quae amet natus, deleniti illum esse dolor voluptas similique est nam nostrum, provident praesentium iure. Nemo?',
            'Ipsum laudantium laboriosam saepe. Quaerat nisi perspiciatis cumque beatae vitae quod qui dignissimos iure distinctio aliquid, at voluptates sint dolor, eius quis nesciunt facilis excepturi recusandae, repellat inventore deserunt ipsam quisquam? Tempore nisi tenetur, voluptas atque. Neque, molestias doloribus non magni libero et soluta assumenda voluptates perspiciatis vitae, omnis beatae aspernatur, obcaecati eos autem nam nulla voluptatem. Esse asperiores temporibus distinctio, quidem debitis cumque ipsum mollitia, eaque! Est sunt voluptas, vel rerum animi odit praesentium unde earum tenetur itaque similique soluta saepe recusandae necessitatibus ipsam reprehenderit, facilis eligendi. Adipisci optio rerum eaque explicabo maiores minus, soluta iusto blanditiis dolorem obcaecati!',
            'Assumenda alias totam, temporibus reprehenderit beatae sint neque consequuntur harum aliquid eaque laudantium eum ratione, distinctio asperiores quam iure atque cupiditate ipsum magnam, iusto ducimus molestias quisquam sapiente itaque incidunt. Atque rerum maiores aut pariatur quas tempore voluptate, amet voluptatum a quam, dignissimos nesciunt necessitatibus aperiam vitae! Facere dolor mollitia, excepturi sequi asperiores pariatur sed consectetur officiis odit totam dolore alias velit, accusantium atque impedit, incidunt reprehenderit provident architecto at vitae. Amet aliquid tenetur sunt eum, pariatur error quae nostrum delectus consequatur dignissimos iste architecto qui, ducimus voluptates aliquam itaque laudantium blanditiis? Cum quos deleniti dignissimos perspiciatis quasi vel officia.'
        ];
    }

    function getPhrases()
    {
        return [
            'Lorem ipsum dolor sit amet',
            'Odio veritatis, hic nihil architecto!',
            'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, reprehenderit!',
            'Et, distinctio dolores tempore magnam.',
            'Lorem ipsum dolor sit.',
            'Maiores, iste minima quidem aperiam!',
            'Ratione magni nobis laboriosam esse cumque. Adipisci praesentium nostrum est.',
            'Debitis facere minima maiores necessitatibus!',
            'Vel inventore, eveniet odio!',
            'Aperiam saepe officia dignissimos neque animi voluptatibus, laudantium suscipit, quos.',
            'Illo tenetur id nam sunt recusandae sed voluptas, nostrum ullam.',
            'Amet aliquam, quam voluptas libero aliquid est molestias natus ex!',
            'Perspiciatis enim nisi ut!',
            'Voluptatum est sit reprehenderit!',
            'Nesciunt ducimus vitae perferendis numquam, nam, velit minus! Dolores, fuga.',
            'Sunt expedita minus a.',
            'Animi quasi reprehenderit, sapiente adipisci?'
        ];
    }

    public function getUsernames()
    {
        return ['Toto', 'Tata', 'Titi'];
    }

    public function getCategoryNames()
    {
        return [
            'Paris',
            'musée',
            'visite',
            'histoire',
            'géographie',
            'art',
            'peinture',
            'musique',
            'métier',
            'transport',
            'orientation',
            'sport'
        ];
    }

    public function getAvatarNames()
    {
        return ['devid', 'black_leopard', 'cat', 'dachshund', 'frog', 'hedgehog', 'koala', 'leopard', 'owl', 'panda', 'pug', 'snow_leopard'];
    }

    public function getPhotoTitles()
    {
        return [
            "Musée d'histoire naturelle",
            "Opéra Garnier",
            "Canal-Saint-Martin",
            "Keith Haring",
            "Notre-Dame de Paris",
            "Tour Eiffel",
            "Sacré Coeur de Montmartre",
            "Ecluse",
            "Musée du Louvre",
            "Carte",
            "Place de la République",
            "Eglise de la Madeleine",
            "Hôtel de Ville de Paris",
            "Place de la Concorde",
        ];
    }
    function getRandomPostContent()
    {
        $contents = $this->getPostContents();
        $randKey = array_rand($contents);
        return $contents[$randKey];
    }

    public function getRandomPostTitles()
    {
        $titles = $this->getPhrases();
        shuffle($titles);
        return array_slice($titles, 0, 14);
    }

    public function getRandomCommentContent()
    {
        $phrases = $this->getPhrases();
        $numPhrases = mt_rand(2, 10);
        shuffle($phrases);
        return implode(' ', array_slice($phrases, 0, $numPhrases));
    }
}
