<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username',  TextType::class, array(
                'label' => 'Votre pseudo :',
                'attr'  => array('data-length' => '30')
            ))
            ->add('firstname', TextType::class, array(
                'label' => 'Votre prénom :',
                'attr'  => array('data-length' => '255')
            ))
            ->add('lastname',  TextType::class, array(
                'label' => 'Votre nom :',
                'attr'  => array('data-length' => '255')
            ))
            ->add('avatar',    EntityType::class, array(
                'required'      => false,
                'class'         => 'AppBundle:Avatar',
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $er)
                {
                    return $er->createQueryBuilder('a');
                },
                'multiple'     => false,
                'expanded'     => true,
                'label'        => 'Choisissez votre avatar :',
                'label_attr'   => array('class' => 'avatar-label')
            ))
            ->add('email',     EmailType::class, array('label' => 'Votre email :'))
            ->add('password',  RepeatedType::class, array(
                'type'            => PasswordType::class,
                'invalid_message' => 'Le nouveau mot de passe doit être identique à la confirmation.',
                'first_options'   => array(
                    'label' => 'Votre mot de passe :',
                    'attr'  => array('data-confirm' => '1')
                ),
                'second_options'  => array(
                    'label' => 'Confirmation du mot de passe :',
                    'attr'  => array('data-confirm' => '2')
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }


}
