<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class AdminUserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('roles',    ChoiceType::class, array(
                'choices'  => array(
                    'Administrateur' => 'ROLE_SUPER_ADMIN',
                    'Editeur'        => 'ROLE_ADMIN',
                    'Auteur'         => 'ROLE_AUTHOR',
                    'Abonné'         => 'ROLE_USER',
                ),
                'multiple' => true,
                'expanded' => true,
                'label'    => 'Autorisations :'
            ))
            ->add('isActive', CheckboxType::class, array(
                'label'    => 'Cochez pour activer le compte',
                'required' => false
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return UserEditType::class;
    }
}
