<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Form\PhotoType;

class PostType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',       TextType::class, array(
                'label' => "Titre de l'article :",
                'attr'  => array('data-length' => '100')
            ))
            ->add('content',     TextareaType::class, array(
                'label' => "Contenu de l'article :"
            ))
            ->add('isPublished', CheckboxType::class, array(
                'required' => false,
                'label'    => "Cochez si vous voulez publier l'article"
            ))
            ->add('save',        SubmitType::class, array(
                'label' => 'Enregistrer'
            ))
            ->add('categories',  EntityType::class, array(
                 'class'        => 'AppBundle:Category',
                 'choice_label' => function($category) {
                     return ucfirst($category->getName());
                 },
                 'multiple'     => true,
                 'label'        => 'Choisissez la ou les catégorie(s) :'
            ))
            ->add('photo',      PhotoType::class, array('required' => false))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Post'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_post';
    }


}
