<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Form\PostType;

class AdminPostType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('author',       EntityType::class, array(
                'class'        => 'AppBundle:User',
                'choice_label' => function($user) {
                    return ucfirst($user->getUsername());
                },
                'label'        => 'Auteur : '
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return PostType::class;
    }
}
