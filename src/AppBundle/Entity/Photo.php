<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Photo
 *
 * @ORM\Table(name="ob_photo")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PhotoRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields="url", message="Cette photo existe déjà.")
 */
class Photo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=20)
     * @Assert\Length(max=20, maxMessage = "Le titre ne doit pas dépasser {{ limit }} caractères.")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, unique=true)
     */
    private $url;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Post",  inversedBy="photo")
     * @ORM\JoinColumn(name="fk_post_id")
     * @Assert\Valid()
     */
    private $post;

    /**
     * @Assert\Image(
     *     mimeTypesMessage = "Le fichier doit être une image.",
     *     uploadIniSizeErrorMessage = "La taille du fichier ne doit pas dépasser {{ limit }} Mo",
     *     uploadFormSizeErrorMessage = "Le fichier est trop gros.",
     *     uploadErrorMessage = "Le téléchargement du fichier a échoué."
     * )
     */
    private $file;

    private $filename;
    // Stockage du nom du fichier temporaire
    private $tempFilename;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Photo
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Photo
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set post
     *
     * @param \AppBundle\Entity\Post $post
     *
     * @return Photo
     */
    public function setPost(\AppBundle\Entity\Post $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \AppBundle\Entity\Post
     */
    public function getPost()
    {
        return $this->post;
    }

    /*
     * UPLOAD DES PHOTOS
     */
    public function getFile()
    {
        return $this->file;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // Test si un fichier est déjà enregistré pour cette entité
        if (is_file($this->getUploadRootDir() . $this->url)) {
            // Sauvegarde du nom présent dans $tempFilename pour pouvoir le supprimer après
            $this->tempFilename = $this->getUploadRootDir() . $this->url;
            // Réinitialisation des attributs
            $this->url = null;
            $this->title = null;
        }
    }

    /**
      * Méthode avant le déplacement du fichier
      * @ORM\PrePersist
      * @ORM\PreUpdate
      */
    public function preUpload()
    {
        // Si il n'y a pas de fichier (champ facultatif)
        if (null === $this->file) {
            return;
        }
        $this->filename = md5(uniqid()) . '.' . $this->file->guessExtension();
        // Url depuis web/
        $this->url = $this->getUploadDir() . $this->filename;
        // Nom original du fichier uploadé par l'utilisateur
        $this->title = $this->file->getClientOriginalName();
    }

    /**
     * Losque l'article est réellement enregistré en bdd on déplace le fichier
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function upload()
    {
        // Si il n'y a pas de fichier (champ facultatif)
        if (null === $this->file) {
            return;
        }
        // Si il y avait déjà un fichier, on le supprime
        if (isset($this->tempFilename)) {
            unlink($this->tempFilename);
            $this->tempFilename = null;
        }
        // Déplacement du fichier uploadé dans le répertoire choisi
        $this->file->move(
            $this->getUploadRootDir() . $this->getUploadDir(),
            $this->filename
        );
        // Suppression des valeurs des attributs non persistés en bdd
        $this->file = null;
        $this->filename = null;
        $this->tempFilename = null;
    }

    /**
     * @ORM\PreRemove()
     */
    public function preRemoveUpload()
    {
        $this->tempFilename = $this->getUploadRootDir() . $this->url;
    }

    /**
     * @ORM\PostRemove()
     */
    public function postRemoveUpload()
    {
        if (isset($this->tempFilename)) {
            unlink($this->tempFilename);
        }
    }

    public function getUploadDir()
    {
        // Chemin relatif vers la photo depuis web/
        return 'uploads/photos/';
    }

    public function getUploadRootDir()
    {
        // Chemin absolu vers l'image
        return __DIR__.'/../../../web/';
    }
}
