<?php
namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Post;
use AppBundle\Entity\Comment;
use AppBundle\Form\AdminPostType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
* @Security("has_role('ROLE_SUPER_ADMIN')")
*/
class PostController extends Controller
{
    public function indexAction()
    {
        $em         = $this->getDoctrine()->getManager();
        $posts      = $em->getRepository('AppBundle:Post')->findAll();
        $categories = $em->getRepository('AppBundle:Category')->findAll();
        $authors    = $em->getRepository('AppBundle:User')->findAll();

        return $this->render('AppBundle:Admin/Post:index.html.twig', array(
            'posts'      => $posts,
            'categories' => $categories,
            'authors'    => $authors
        ));
    }

    public function editAction(Post $post, Request $request)
    {
        $formPost = $this->createForm(AdminPostType::class, $post);
        $formPost->handleRequest($request);
        if ($formPost->isSubmitted() && $formPost->isValid()) {
            if ($post->getIsPublished()) {
                $post->setPublishedAt(new \Datetime());
            }
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('notice', "L'article '". $post->getTitle() . "' a été modifié.");
            return $this->redirectToRoute('admin_post');
        }
        return $this->render('AppBundle:Admin/Post:edit.html.twig', array(
            'post' => $post,
            'formPost' => $formPost->createView()
        ));
    }

    /**
     * @ParamConverter("post", options={"mapping": {"post_id": "id"}})
     * @ParamConverter("comment", options={"mapping": {"comment_id": "id"}})
     */
    public function deleteCommentAction(Post $post, Comment $comment, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($comment);
        $em->flush();
        $this->addFlash('notice', "Le commentaire a été supprimé.");
        return $this->redirectToRoute('post_view', ['id' => $post->getId()]);
    }

    public function searchAction(Request $request, $criteria, $id)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException("La page demandée n'existe pas.");
        }
        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Post');
        if ($id == 0 && $criteria == null) {
            $posts = $repository->findAll();
        } else {
            if ($criteria == 'author') {
                $posts = $repository->findByAuthor($id);
            } else {
                $posts = $repository->findByCategory([$id], false);
            }
        }
        return $this->render('AppBundle:layout:tablePost.html.twig', array('posts' => $posts));
    }
}
