<?php
namespace AppBundle\Controller\Admin;

use AppBundle\Entity\User;
use AppBundle\Form\AdminUserType;
use AppBundle\Service\PasswordGenerator;
use AppBundle\Service\AppMailer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
* @Security("has_role('ROLE_SUPER_ADMIN')")
*/
class UserController extends Controller
{
    public function indexAction()
    {
        $users = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:User')
            ->findAll()
        ;

        return $this->render('AppBundle:Admin/User:index.html.twig', array('users' => $users));
    }

    public function newAction(Request $request, UserPasswordEncoderInterface $encoder, PasswordGenerator $passwordGenerator, AppMailer $mailer)
    {
        $user = new User();

        $form = $this->createForm(AdminUserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $plainPass = $passwordGenerator->getRandomPassword();
            $encodedPass = $encoder->encodePassword($user, $plainPass);
            $user->setPassword($encodedPass);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            // Envoi d'une notification d'inscription
            $mailer->notifyRegistration($user, $plainPass);

            $this->addFlash('notice', "Nouvel utilisateur " . $user->getUsername() . " inscrit");
            return $this->redirectToRoute('admin_user');
        }
        return $this->render('AppBundle:Admin/User:new.html.twig', array('form' => $form->createView()));
    }

    public function editAction(Request $request, User $user)
    {
        $form = $this->createForm(AdminUserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $this->addFlash('notice', "Utilisateur " . $user->getUsername() ." modifié");
            return $this->redirectToRoute('admin_user');
        }
        return $this->render('AppBundle:Admin/User:edit.html.twig', array(
            'user' => $user,
            'form' => $form->createView()
        ));
    }
}
