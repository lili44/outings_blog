<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\Category;
use AppBundle\Entity\Photo;
use AppBundle\Form\PostType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class PostController extends Controller
{
    public function indexAction($page)
    {
        if ($page < 1) {
            throw new NotFoundHttpException('Page ' . $page . ' non existante.');
        }
        //  PAGINATION
        $nbPerPage = 5;
        // Récupération de l'EntityManager
        $em = $this->getDoctrine()->getManager();
        // Récupération de la liste des entités Post
        $posts = $em->getRepository('AppBundle:Post')->getPostsPublished($page, $nbPerPage);

        $nbPages = ceil(count($posts) / $nbPerPage);
        if ($page > $nbPerPage) {
            throw new NotFoundHttpException('Page '.$page.' non existante.');
        }
        $categories = $em->getRepository('AppBundle:Category')->findAll();
        $authors    = $em->getRepository('AppBundle:User')->findBy(
            array('isActive' => true)
        );

        // Retourne $posts pour afficher dans la vue
        return $this->render(
            'AppBundle:Post:index.html.twig',
            array(
                'posts'       => $posts,
                'nbPages'     => $nbPages,
                'page'        => $page,
                'categories'  => $categories,
                'authors'     => $authors,
                'currentPage' => 'blog'
        ));
    }

    public function viewAction(Post $post)
    {
        return $this->render(
            'AppBundle:Post:view.html.twig',
            array(
                'post'        => $post,
                'currentPage' => 'view-post'
            )
        );
    }

    /**
     * @Security("has_role('ROLE_USER')")
     */
    public function addAction(Request $request)
    {
        // Création d'un objet Post
        $post = new Post();
        // Création du formulaire
        // $formPost = $this->get('form.factory')->create(PostType::class, $post);
        // OU
        $formPost = $this->createForm(PostType::class, $post);
        // Persistance en bdd si on récupère le résultat d'une requête POST
        if ($request->isMethod('POST') && $formPost->handleRequest($request)->isValid()) {
            if ($post->getIsPublished() === true) {
                $post->setPublishedAt(new \Datetime());
            }
            $post->setAuthor($this->getUser());
            // Persistence en base de données de l'article
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();
            // Création du message flash informant l'utilisateur du succès
            $this->addFlash('notice', 'Article bien enregistré.');
            // Affichage du message flash dans la vue
            return $this->redirectToRoute('post_home');
        }
        // Si on ne récupère pas de requête POST on affiche le formulaire
        return $this->render(
            'AppBundle:Post:add.html.twig', array(
                'formPost' => $formPost->createView(),
                'currentPage' => 'newPost'
            )
        );

    }

    /**
     * @Security("post.isAuthor(user) or has_role('ROLE_SUPER_ADMIN')")
     */
    public function editAction(Request $request, Post $post)
    {
        // Création du formulaire prérempli
        $formPost = $this->createForm(PostType::class, $post);
        $formPost->handleRequest($request);
        if ($formPost->isSubmitted() && $formPost->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($post->getIsPublished() === true) {
                $post->setPublishedAt(new \Datetime());
            }
            $em->flush();
            // Message flash
            $this->addFlash('notice', 'Article modifié.');
            return $this->redirectToRoute('user_post', array('id' => $this->getUser()->getId()));
        }
        // Sinon, affichage du formulaire prérempli
        return $this->render(
            'AppBundle:Post:edit.html.twig',
            array('formPost' => $formPost->createView(), 'post' => $post)
        );
    }

    /**
    * @Security("post.isAuthor(user) or has_role('ROLE_SUPER_ADMIN')")
    */
    public function deleteAction(Request $request, Post $post)
    {
        // Création d'une variable $postTitle pour pouvoir afficher le nom de l'article supprimé dans le message flash
        $postTitle = null;
        // Création d'un formulaire pour insérer le champ CSRF
        $form =  $this->createFormBuilder()->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $postTitle = $post->getTitle();
            $em = $this->getDoctrine()->getManager();
            $em->remove($post);
            $em->flush();
            $this->addFlash('notice', "L'article '$postTitle' a été supprimé.");

            if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
                return $this->redirectToRoute('admin_post');
            } else {
                return $this->redirectToRoute(
                    'user_post',
                    array('id' => $this->getUser()->getId())
                );
            }
        }

        return $this->render(
            'AppBundle:Post:delete.html.twig',
            array(
                'post' => $post,
                'form' => $form->createView()
            )
        );
    }

    // Affichage des dernières photos dans un slider -> app_homepage
    public function sliderAction($limit)
    {
        // Récupération de photo des derniers articles
        $listPosts = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Post')
            ->getPostWithPhoto($limit)
        ;
        // Vérification que le fichier photo de chaque post existe
        foreach ($listPosts as $post) {
            if ($post->getPhoto() !== null) {
                $filename = $this->getParameter('web_directory') . $post->getPhoto()->getUrl();
                if (!file_exists($filename)) {
                    $post->getPhoto()->setUrl('uploads/no-photo.svg');
                }
            }
        }
        return $this->render(
            'AppBundle:Post:slider.html.twig',
            array('listPosts' => $listPosts)
        );
    }

    // Affichage des derniers posts dans le footer
    public function menuAction($limit)
    {
        // Récupération des derniers posts pour afficher dans le footer
        $lastPosts = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Post')
            ->getPostTitle($limit)
        ;
        return $this->render(
            'AppBundle:Post:menu.html.twig',
            array('lastPosts' => $lastPosts)
        );
    }

    public function viewByCategoryAction(Category $category, $page)
    {
        if ($page < 1) {
            throw new NotFoundHttpException('Page ' . $page . ' non existante.');
        }
        //  PAGINATION
        $nbPerPage = 5;

        // Récupération des articles correspondant à $category
        $listPosts = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Post')
            ->findPublishedByCategory([$category->getId()], $page, $nbPerPage)
        ;
        $nbPages = ceil(count($listPosts) / $nbPerPage);
        if ($page > $nbPerPage) {
            throw new NotFoundHttpException('Page '.$page.' non existante.');
        }
        return $this->render(
            'AppBundle:Post:byCategory.html.twig',
            array(
                'page'        => $page,
                'nbPages'     => $nbPages,
                'category'    => $category,
                'listPosts'   => $listPosts,
                'currentPage' => 'by-category'
            )
        );
    }

    public function searchAction(Request $request, $criteria, $id)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException("La page demandée n'existe pas.");
        }

        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Post');
        if ($id == 0) {
            $posts = $repository->getPostWithPhoto();
        } else {
            if ($criteria == 'author') {
                $posts = $repository->findBy(
                    [
                        'author'      => $id,
                        'isPublished' => true
                    ],
                    ['publishedAt' => 'desc']
                );
            } else {
                $posts = $repository->findByCategory([$id]);
            }
        }
        return $this->render('AppBundle:Post:search.html.twig', array(
            'posts'       => $posts,
            'currentPage' => 'search-post'
        ));
    }
}
