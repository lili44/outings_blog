<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\Comment;
use AppBundle\Entity\Contact;
use AppBundle\Form\CommentType;
use AppBundle\Form\ContactType;
use AppBundle\Service\AppMailer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use ReCaptcha\ReCaptcha;
use ReCaptcha\RequestMethod;

class BlogController extends Controller
{
    public function indexAction()
    {
        return $this->render('AppBundle:Blog:index.html.twig', array(
            'currentPage'    => 'home',
            'listCategories' => $this->showCategories()
        ));
    }

    public function aboutAction()
    {
        return $this->render('AppBundle:Blog:about.html.twig', array('currentPage' => 'about'));
    }

    public function downloadAction($file, $format)
    {
        return $this->file(
            $this->getParameter('download_directory') . '/' . $file . '.' . $format,
            'EliseBorie_DeveloppeuseWeb.pdf',
            ResponseHeaderBag::DISPOSITION_INLINE
        );
    }

    public function commentAction(Request $request, Post $post)
    {
        $comment = new Comment();
        $post->addComment($comment);
        $formComment = $this->createForm(CommentType::class, $comment, array(
            'action' => $this->generateUrl(
                'comment_add',
                array('id' => $post->getId())
                )
        ));
        $formComment->handleRequest($request);
        if ($formComment->isSubmitted() && $formComment->isValid()) {
            $recaptcha = new ReCaptcha('6LdBoDcUAAAAACp10_T5ug9dW23JN_Z8IaSugb40', new RequestMethod\CurlPost());
            $resp = $recaptcha->verify($request->request->get('g-recaptcha-response'), $request->getClientIp());
            if (!$resp->isSuccess()) {
                $this->addFlash('warning', "Le reCAPTCHA ne vous a pas identifié comme humain. Réessayer.");
                return $this->redirectToRoute(
                    'post_view',
                    array(
                        'id' => $post->getId()
                    )
                );
            }else{
                $comment->setAuthor($this->getUser());
                $em = $this->getDoctrine()->getManager();
                // Persistance du commentaire
                $em->persist($comment);
                $em->flush();
                // Création du message flash
                $this->addFlash('notice', 'Votre commentaire a été enregistré.');
                return $this->redirectToRoute(
                    'post_view',
                    array('id' => $post->getId())
                );
            }
        }
        // Affichage du formulaire
        return $this->render(
            'AppBundle:Blog:commentForm.html.twig',
            array(
                'post'        => $post,
                'formComment' => $formComment->createView()
                )
        );
    }

    public function galleryAction()
    {
        $listPhotos = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('AppBundle:Photo')
        ->getPhotoPublished()
        ;
        return $this->render(
            'AppBundle:Blog:gallery.html.twig', array(
                'listPhotos' => $listPhotos,
                'currentPage' => 'gallery'
            )
        );
    }

    public function showCategories()
    {
        $listCategories = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Category')
            ->findAll()
        ;
        return $listCategories;
    }

    public function contactAction(Request $request, AppMailer $mailer)
    {
        $contact = new Contact();
        if ($this->getUser() !== null) {
            $user = $this->getUser();
            $contact->setEmail($user->getEmail());
            $contact->setName($user->getUsername());
            $contact->setUser($this->getUser());
        }
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $recaptcha = new ReCaptcha('6LdGqzcUAAAAAAULczZTIv2PayM7Ut0iN9xXv5Jd', new RequestMethod\CurlPost());
            $resp = $recaptcha->verify($request->request->get('g-recaptcha-response'), $request->getClientIp());
            if (!$resp->isSuccess()) {
                $this->addFlash('warning', "Le reCAPTCHA ne vous a pas identifié comme humain. Réessayer.");
                return $this->redirectToRoute('app_contact');
            } else {
                $em = $this->getDoctrine()->getManager();
                $em->persist($contact);
                $em->flush();
                // Envoi du mail à l'administrateur du site
                $mailer->sendContactMessage($contact);

                $this->addFlash('notice', "Votre message a été envoyé.");
                return $this->redirectToRoute('app_homepage');
            }
        }
        return $this->render('AppBundle:Blog:contact.html.twig', array(
            'form' => $form->createView(),
            'currentPage' => 'contact'
            )
        );
    }
}
