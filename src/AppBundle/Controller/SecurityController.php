<?php
namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Service\PasswordGenerator;
use AppBundle\Service\AppMailer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class SecurityController extends Controller
{
    public function loginAction(Request $request)
    {
        // Test si user déjà authentifié
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('app_homepage');
        }

        $authUtils = $this->get('security.authentication_utils');

        return $this->render('AppBundle:Security:login.html.twig', array(
            'last_username' => $authUtils->getLastUsername(),
            'error'         => $authUtils->getLastAuthenticationError(),
            'currentPage'   => 'login'
        ));
    }

    /**
     * @ParamConverter("user", options={"mapping": {"user_id": "id"}})
     */
    public function registrationValidatorAction(User $user, $key)
    {
        if ($user->getRegistrationKey() !== $key ) {
            throw new NotFoundHttpException("La page à laquelle vous souhaitez accéder n'existe pas.");
        }

        $user->setIsActive(true);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        return $this->redirectToRoute('login');
    }

    public function resetPasswordAction(Request $request, UserPasswordEncoderInterface $encoder, AppMailer $mailer, PasswordGenerator $passwordGenerator)
    {
        // REINITIALISATION DU MOT DE PASSE DANS UNE MODALE
        // $username = $request->request->get('username');
        // if (!$request->isXmlHttpRequest() || $username == null) {
        //     $response = new Response();
        //     $response->setContent("La page demandée n'existe pas");
        //     $response->setStatusCode(Response::HTTP_NOT_FOUND);
        //     return $response;
        //
        // }

        $form = $this->createFormBuilder()
            ->add('username', TextType::class, array(
                'label' => 'Votre pseudo',
                'attr'  => array('data-length' => '30')
            ))
            ->add('save',     SubmitType::class, array('label' => 'Envoyer'))
            ->getForm()
        ;
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('AppBundle:User')->findOneByUsername($form->get('username')->getData());
            if ($user === null || $user->getIsActive() === false) {
                $this->addFlash('warning', "Le pseudo n'existe pas.");
                return $this->redirectToRoute('login');
                // $data = ['error' => false];
            }
            // else {
            //     $data = ['error' => true];
            $randomPassword = $passwordGenerator->getRandomPassword();
            $encodedPass = $encoder->encodePassword($user, $randomPassword);
            $user->setPassword($encodedPass);
            $em->flush();
            // Envoi d'un email à l'utilisateur
            $mailer->sendNewPassword($user, $randomPassword);
            // }

            $this->addFlash('notice', "Un nouveau mot de passe vous a été envoyé par mail.");
            return $this->redirectToRoute('login');

            // REPONSE DE LA REQUETE AJAX
            // $response = new Response(json_encode($data));
            // $response->headers->set('Content-Type', 'application/json');
            // return $response;
        }
        return $this->render('AppBundle:Security:resetPassword.html.twig', array('form' => $form->createView()));
    }
}
