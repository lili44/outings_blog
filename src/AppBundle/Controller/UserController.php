<?php
namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\Category;
use AppBundle\Form\UserType;
use AppBundle\Form\UserEditType;
use AppBundle\Service\AppMailer;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use ReCaptcha\ReCaptcha;
use ReCaptcha\RequestMethod;

class UserController extends Controller
{
    /**
     * @Security("askedUser.isUser(user) or has_role('ROLE_SUPER_ADMIN')")
     */
    public function showAllPostsAction(User $askedUser)
    {
        $em = $this->getDoctrine()->getManager();
        $posts = $em->getRepository('AppBundle:Post')->findBy(
            ['author' => $askedUser->getId()],
            ['updatedAt' => 'desc']
        );
        $categories = $em->getRepository('AppBundle:Category')->findAll();

        return $this->render('AppBundle:User:post.html.twig', array(
            'user'        => $askedUser,
            'posts'       => $posts,
            'categories'  => $categories,
            'currentPage' => 'myPosts'
        ));
    }


    public function registerAction(Request $request, UserPasswordEncoderInterface $encoder, AppMailer $mailer)
    {
        $user = new User();

        $formUser = $this->createForm(UserType::class, $user);
        $formUser->handleRequest($request);
        if ($formUser->isSubmitted() && $formUser->isValid()) {
            $recaptcha = new ReCaptcha('6LdUHDgUAAAAADfwbn0fmlYwPA1JzIlTW-qZpJcO', new RequestMethod\CurlPost());
            $resp = $recaptcha->verify($request->request->get('g-recaptcha-response'), $request->getClientIp());
            if ($resp->isSuccess()) {
                $plainPass = $user->getPassword();
                $encodedPass = $encoder->encodePassword($user, $plainPass);
                $user->setPassword($encodedPass);
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $url = $this->generateUrl(
                    'user_registration_validator',
                    array(
                        'user_id' => $user->getId(),
                        'key'     => $user->getRegistrationKey()
                    ),
                    UrlGeneratorInterface::ABSOLUTE_URL
                );
                // Envoi d'un email à l'utilisateur
                $mailer->sendRegistrationValidator($user, $url);

                $this->addFlash('notice', "Vous allez recevoir un email pour confimer votre inscription.");
                return $this->redirectToRoute('app_homepage');
            } else {
                $this->addFlash('warning', "Le reCAPTCHA ne vous pas identifié comme humain. Veuillez recommencez.");
                return $this->redirectToRoute('user_signup');
            }
        }

        return $this->render(
            'AppBundle:User:register.html.twig', array(
                'formUser' => $formUser->createView(),
                'currentPage' => 'registration'
            )
        );
    }

    /**
     * @Security("askedUser.isUser(user) or has_role('ROLE_SUPER_ADMIN')")
     */
    public function showAction(User $askedUser)
    {
        $authorization = "de visualiser les articles et écrire des commentaires.";
        if (in_array('ROLE_SUPER_ADMIN', $askedUser->getRoles())) {
            $authorization = "de modifier et supprimer tous les articles, commentaires et comptes d'utilisateur.";
        } else if (in_array('ROLE_USER', $askedUser->getRoles())) {
            $authorization = "d'écrire des articles, les modifier et les supprimer. Vous avez accès à votre compte et la possibilité de le modifier ou le supprimer.";
        }

        return $this->render('AppBundle:User:account.html.twig', array(
            'user' => $askedUser,
            'authorization' => $authorization,
            'currentPage' => 'myAccount'
        ));
    }

    /**
     * @Security("askedUser.isUser(user) or has_role('ROLE_SUPER_ADMIN')")
     */
    public function editAction(Request $request, User $askedUser)
    {
        $formUser = $this->createForm(UserEditType::class, $askedUser);
        $formUser->handleRequest($request);
        if ($formUser->isSubmitted() && $formUser->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('notice', 'Vos modifications ont été prises en compte.');
            return $this->redirectToRoute(
                'user_show',
                array('id' => $askedUser->getId())
            );
        }
        return $this->render(
            'AppBundle:User:edit.html.twig',
            array(
                'formUser' => $formUser->createView(),
                'user'     => $askedUser
            )
        );
    }

    /**
     * @Security("askedUser.isUser(user) or has_role('ROLE_SUPER_ADMIN')")
     */
    public function deleteAction(Request $request, User $askedUser)
    {
        // Récupération du username pour pouvoir l'afficher dans le message flash.
        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $askedUser->setIsActive(false);
            $posts = $askedUser->getPosts();
            foreach ($posts as $post) {
                $post->setIsPublished(false);
            }
            $username = $askedUser->getUsername();
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
                $this->addFlash('notice', "L'utilisateur '$username' a été désactivé.");
                return $this->redirectToRoute('admin_user');
            } else {
                $this->addFlash('notice', "Votre compte a été supprimé.");
                return $this->redirectToRoute('logout');
            }
        }

        return $this->render(
            'AppBundle:User:delete.html.twig',
            array(
                'user' => $askedUser,
                'form' => $form->createView()
            )
        );
    }

    /**
     * @Security("askedUser.isUser(user)")
     */
    public function changePasswordAction(Request $request, User $askedUser, UserPasswordEncoderInterface $encoder)
    {
        $form = $this->createFormBuilder($askedUser)
            ->add('oldPass',     PasswordType::class, array('label' => 'Ancien mot de passe :'))
            ->add('newPass',     RepeatedType::class, array(
                'type'            => PasswordType::class,
                'invalid_message' => 'Le nouveau mot de passe doit être identique à la confirmation.',
                'first_options'   => array(
                    'label' => 'Nouveau mot de passe :',
                    'attr'  => array('data-confirm' => '1')
                ),
                'second_options'  => array(
                    'label' => 'Confirmation du mot de passe :',
                    'attr'  => array('data-confirm' => '2')
                )
            ))
            ->add('save',        SubmitType::class, array('label' => 'Enregistrer'))
            ->getForm()
        ;
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $oldPass = $form->getData()->getOldpass();
            if ($encoder->isPasswordValid($askedUser, $oldPass)) {
                $newPass = $form->getData()->getNewpass();
                $encodedPass = $encoder->encodePassword($askedUser, $newPass);
                $askedUser->setPassword($encodedPass);
                $em = $this->getDoctrine()->getManager();
                $em->flush();

                $this->addFlash('notice', 'Nouveau mot de passe enregistré.');
                return $this->redirectToRoute('user_edit', array('id' => $askedUser->getId()));
            }
            $this->addFlash('warning', 'Mot de passe invalide.');
        }
        return $this->render('AppBundle:User:changePass.html.twig', array(
            'user' => $askedUser,
            'form' => $form->createView()
        ));
    }

    /**
     * @ParamConverter("askedUser", options={"mapping": {"user_id": "id"}})
     * @Security("askedUser.isUser(user)")
     */
    public function searchAction(Request $request, User $askedUser, $cat_id)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException("La page demandée n'existe pas");
        }

        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Post');
        if ($cat_id === 0) {
            $posts = $repository->findByAuthor($askedUser);
        } else {
            $posts = $repository->findByCategoryAuthor([$cat_id], $askedUser);
        }
        return $this->render('AppBundle:layout:tablePost.html.twig', array('posts' => $posts));
    }
}
