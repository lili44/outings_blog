<?php
namespace AppBundle\Service;

use Symfony\Component\Templating\EngineInterface;

class AppMailer
{
    private $mailer;
    private $templating;
    private $adminEmail;

    function __construct(\Swift_Mailer $mailer, EngineInterface $templating, $adminEmail)
    {
        $this->mailer     = $mailer;
        $this->templating = $templating;
        $this->adminEmail = $adminEmail;
    }

    public function sendMessage($from, $to, $subject, $body)
    {
        $message = (new \Swift_Message($subject))
            ->setFrom($from)
            ->setTo($to)
            ->setBody($body)
            ->setContentType('text/html')
            ->setCharset('UTF-8')
        ;
        $this->mailer->send($message);
    }

    public function sendContactMessage($contact)
    {
        $from    = $contact->getEmail();
        $to      = $this->adminEmail;
        $subject = "Nouveau message de L'école prend l'air";
        $body    = $this->templating->render(
            'AppBundle:Email:contact.html.twig',
            array('contact' => $contact)
        );

        $this->sendMessage($from, $to, $subject, $body);
    }

    public function sendRegistrationValidator($user, $url)
    {
        $from    = $this->adminEmail;
        $to      = $user->getEmail();
        $subject = "Activation de votre compte sur le site L'école prend l'air";
        $body    = $this->templating->render(
            'AppBundle:Email:registrationValidator.html.twig',
            array(
                'user' => $user,
                'url'  => $url
            )
        );
        $this->sendMessage($from, $to, $subject, $body);
    }

    public function sendNewPassword($user, $password)
    {
        $from    = $this->adminEmail;
        $to      = $user->getEmail();
        $subject = "Votre nouveau mot de passe";
        $body    = $this->templating->render(
            'AppBundle:Email:resetPassword.html.twig',
            array(
                'user'      => $user,
                'password'  => $password
            )
        );
        $this->sendMessage($from, $to, $subject, $body);
    }

    public function notifyRegistration($user, $password)
    {
        $from    = $this->adminEmail;
        $to      = $user->getEmail();
        $subject = "Votre inscription sur le site L'école prend l'air";
        $body    = $this->templating->render(
            'AppBundle:Email:notifyRegistration.html.twig',
            array(
                'user'      => $user,
                'password'  => $password
            )
        );
        $this->sendMessage($from, $to, $subject, $body);
    }
}
