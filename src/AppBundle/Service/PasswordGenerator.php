<?php
namespace AppBundle\Service;

class PasswordGenerator
{
    const ALPHA         = 'abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ';
    const NUM           = '234567890';
    const SPECIAL_CHARS = '{}&!@#$-%*';

    public function getRandomPassword($length = 8)
    {
        $chars = self::ALPHA . self::NUM . SELF::SPECIAL_CHARS;
        $randomChars = substr(str_shuffle($chars), 0, $length - 3);
        return str_shuffle(
            $randomChars .
            $this->getOneAlpha() .
            $this->getOneNumeric() .
            $this->getOneSpecial()
        );
    }

    public function getOneAlpha()
    {
        return substr(str_shuffle(self::ALPHA), 0, 1);
    }
    public function getOneNumeric()
    {
        return substr(str_shuffle(self::NUM), 0, 1);
    }
    public function getOneSpecial()
    {
        return substr(str_shuffle(self::SPECIAL_CHARS), 0, 1);
    }
}
