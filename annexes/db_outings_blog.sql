-- phpMyAdmin SQL Dump
-- version 4.1.14.8
-- http://www.phpmyadmin.net
--
-- Client :  db711401275.db.1and1.com
-- Généré le :  Mer 29 Novembre 2017 à 13:54
-- Version du serveur :  5.5.57-0+deb7u1-log
-- Version de PHP :  5.4.45-0+deb7u11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `db711401275`
--
-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`) VALUES
('20171125223915');

-- --------------------------------------------------------

--
-- Structure de la table `ob_avatar`
--

CREATE TABLE IF NOT EXISTS `ob_avatar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8535B7CFF47645AE` (`url`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Contenu de la table `ob_avatar`
--

INSERT INTO `ob_avatar` (`id`, `name`, `url`) VALUES
(13, 'devid', 'img/avatar/avatar0.png'),
(14, 'black_leopard', 'img/avatar/avatar1.png'),
(15, 'cat', 'img/avatar/avatar2.png'),
(16, 'dachshund', 'img/avatar/avatar3.png'),
(17, 'frog', 'img/avatar/avatar4.png'),
(18, 'hedgehog', 'img/avatar/avatar5.png'),
(19, 'koala', 'img/avatar/avatar6.png'),
(20, 'leopard', 'img/avatar/avatar7.png'),
(21, 'owl', 'img/avatar/avatar8.png'),
(22, 'panda', 'img/avatar/avatar9.png'),
(23, 'pug', 'img/avatar/avatar10.png'),
(24, 'snow_leopard', 'img/avatar/avatar11.png');

-- --------------------------------------------------------

--
-- Structure de la table `ob_category`
--

CREATE TABLE IF NOT EXISTS `ob_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B33C4F505E237E06` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Contenu de la table `ob_category`
--

INSERT INTO `ob_category` (`id`, `name`) VALUES
(18, 'art'),
(17, 'géographie'),
(16, 'histoire'),
(21, 'métier'),
(14, 'musée'),
(20, 'musique'),
(23, 'orientation'),
(13, 'Paris'),
(19, 'peinture'),
(24, 'sport'),
(22, 'transport'),
(15, 'visite');

-- --------------------------------------------------------

--
-- Structure de la table `ob_comment`
--

CREATE TABLE IF NOT EXISTS `ob_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_user_id` int(11) DEFAULT NULL,
  `fk_post_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `rate` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_34EDF2D15741EEB9` (`fk_user_id`),
  KEY `IDX_34EDF2D1BBA63E00` (`fk_post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=47 ;

--
-- Contenu de la table `ob_comment`
--

INSERT INTO `ob_comment` (`id`, `fk_user_id`, `fk_post_id`, `created_at`, `content`, `rate`) VALUES
(20, NULL, 16, '2017-11-29 13:53:25', 'Ratione magni nobis laboriosam esse cumque. Adipisci praesentium nostrum est. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, reprehenderit!', NULL),
(21, NULL, 16, '2017-11-29 13:53:26', 'Et, distinctio dolores tempore magnam. Lorem ipsum dolor sit amet Illo tenetur id nam sunt recusandae sed voluptas, nostrum ullam. Vel inventore, eveniet odio!', NULL),
(22, NULL, 16, '2017-11-29 13:53:27', 'Debitis facere minima maiores necessitatibus! Perspiciatis enim nisi ut! Voluptatum est sit reprehenderit! Sunt expedita minus a. Lorem ipsum dolor sit. Nesciunt ducimus vitae perferendis numquam, nam, velit minus! Dolores, fuga.', NULL),
(23, NULL, 19, '2017-11-29 13:53:25', 'Aperiam saepe officia dignissimos neque animi voluptatibus, laudantium suscipit, quos. Illo tenetur id nam sunt recusandae sed voluptas, nostrum ullam. Voluptatum est sit reprehenderit! Sunt expedita minus a. Nesciunt ducimus vitae perferendis numquam, nam, velit minus! Dolores, fuga. Vel inventore, eveniet odio! Ratione magni nobis laboriosam esse cumque. Adipisci praesentium nostrum est. Et, distinctio dolores tempore magnam.', NULL),
(24, NULL, 19, '2017-11-29 13:53:26', 'Sunt expedita minus a. Odio veritatis, hic nihil architecto! Maiores, iste minima quidem aperiam! Illo tenetur id nam sunt recusandae sed voluptas, nostrum ullam. Perspiciatis enim nisi ut! Aperiam saepe officia dignissimos neque animi voluptatibus, laudantium suscipit, quos. Lorem ipsum dolor sit. Ratione magni nobis laboriosam esse cumque. Adipisci praesentium nostrum est. Animi quasi reprehenderit, sapiente adipisci?', NULL),
(25, 8, 19, '2017-11-29 13:53:27', 'Aperiam saepe officia dignissimos neque animi voluptatibus, laudantium suscipit, quos. Debitis facere minima maiores necessitatibus! Nesciunt ducimus vitae perferendis numquam, nam, velit minus! Dolores, fuga. Voluptatum est sit reprehenderit! Amet aliquam, quam voluptas libero aliquid est molestias natus ex! Animi quasi reprehenderit, sapiente adipisci? Sunt expedita minus a. Odio veritatis, hic nihil architecto! Maiores, iste minima quidem aperiam! Et, distinctio dolores tempore magnam.', NULL),
(26, 10, 21, '2017-11-29 13:53:25', 'Maiores, iste minima quidem aperiam! Amet aliquam, quam voluptas libero aliquid est molestias natus ex!', NULL),
(27, 10, 21, '2017-11-29 13:53:26', 'Ratione magni nobis laboriosam esse cumque. Adipisci praesentium nostrum est. Debitis facere minima maiores necessitatibus!', NULL),
(28, 9, 22, '2017-11-29 13:53:25', 'Sunt expedita minus a. Vel inventore, eveniet odio! Lorem ipsum dolor sit amet Voluptatum est sit reprehenderit! Aperiam saepe officia dignissimos neque animi voluptatibus, laudantium suscipit, quos. Maiores, iste minima quidem aperiam! Illo tenetur id nam sunt recusandae sed voluptas, nostrum ullam. Nesciunt ducimus vitae perferendis numquam, nam, velit minus! Dolores, fuga. Perspiciatis enim nisi ut! Ratione magni nobis laboriosam esse cumque. Adipisci praesentium nostrum est.', NULL),
(29, NULL, 23, '2017-11-29 13:53:25', 'Ratione magni nobis laboriosam esse cumque. Adipisci praesentium nostrum est. Lorem ipsum dolor sit amet Animi quasi reprehenderit, sapiente adipisci? Nesciunt ducimus vitae perferendis numquam, nam, velit minus! Dolores, fuga.', NULL),
(30, NULL, 24, '2017-11-29 13:53:25', 'Maiores, iste minima quidem aperiam! Animi quasi reprehenderit, sapiente adipisci? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, reprehenderit! Perspiciatis enim nisi ut! Ratione magni nobis laboriosam esse cumque. Adipisci praesentium nostrum est. Lorem ipsum dolor sit amet Odio veritatis, hic nihil architecto! Voluptatum est sit reprehenderit!', NULL),
(31, 8, 25, '2017-11-29 13:53:25', 'Nesciunt ducimus vitae perferendis numquam, nam, velit minus! Dolores, fuga. Voluptatum est sit reprehenderit! Perspiciatis enim nisi ut! Odio veritatis, hic nihil architecto! Maiores, iste minima quidem aperiam! Ratione magni nobis laboriosam esse cumque. Adipisci praesentium nostrum est.', NULL),
(32, NULL, 25, '2017-11-29 13:53:26', 'Amet aliquam, quam voluptas libero aliquid est molestias natus ex! Et, distinctio dolores tempore magnam. Sunt expedita minus a. Lorem ipsum dolor sit. Ratione magni nobis laboriosam esse cumque. Adipisci praesentium nostrum est.', NULL),
(33, 8, 25, '2017-11-29 13:53:27', 'Illo tenetur id nam sunt recusandae sed voluptas, nostrum ullam. Amet aliquam, quam voluptas libero aliquid est molestias natus ex! Vel inventore, eveniet odio!', NULL),
(34, NULL, 25, '2017-11-29 13:53:28', 'Lorem ipsum dolor sit amet Et, distinctio dolores tempore magnam. Sunt expedita minus a.', NULL),
(35, NULL, 25, '2017-11-29 13:53:29', 'Vel inventore, eveniet odio! Odio veritatis, hic nihil architecto! Ratione magni nobis laboriosam esse cumque. Adipisci praesentium nostrum est.', NULL),
(36, NULL, 26, '2017-11-29 13:53:25', 'Maiores, iste minima quidem aperiam! Animi quasi reprehenderit, sapiente adipisci? Lorem ipsum dolor sit. Amet aliquam, quam voluptas libero aliquid est molestias natus ex! Illo tenetur id nam sunt recusandae sed voluptas, nostrum ullam. Lorem ipsum dolor sit amet Perspiciatis enim nisi ut! Aperiam saepe officia dignissimos neque animi voluptatibus, laudantium suscipit, quos.', NULL),
(37, 8, 26, '2017-11-29 13:53:26', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, reprehenderit! Ratione magni nobis laboriosam esse cumque. Adipisci praesentium nostrum est. Perspiciatis enim nisi ut!', NULL),
(38, NULL, 26, '2017-11-29 13:53:27', 'Animi quasi reprehenderit, sapiente adipisci? Amet aliquam, quam voluptas libero aliquid est molestias natus ex! Odio veritatis, hic nihil architecto! Et, distinctio dolores tempore magnam. Illo tenetur id nam sunt recusandae sed voluptas, nostrum ullam. Aperiam saepe officia dignissimos neque animi voluptatibus, laudantium suscipit, quos. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, reprehenderit! Nesciunt ducimus vitae perferendis numquam, nam, velit minus! Dolores, fuga.', NULL),
(39, 10, 26, '2017-11-29 13:53:28', 'Nesciunt ducimus vitae perferendis numquam, nam, velit minus! Dolores, fuga. Lorem ipsum dolor sit. Et, distinctio dolores tempore magnam. Voluptatum est sit reprehenderit! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, reprehenderit! Perspiciatis enim nisi ut! Sunt expedita minus a. Vel inventore, eveniet odio! Lorem ipsum dolor sit amet Ratione magni nobis laboriosam esse cumque. Adipisci praesentium nostrum est.', NULL),
(40, NULL, 26, '2017-11-29 13:53:29', 'Vel inventore, eveniet odio! Odio veritatis, hic nihil architecto! Lorem ipsum dolor sit. Voluptatum est sit reprehenderit!', NULL),
(41, 10, 27, '2017-11-29 13:53:25', 'Lorem ipsum dolor sit. Odio veritatis, hic nihil architecto! Ratione magni nobis laboriosam esse cumque. Adipisci praesentium nostrum est. Voluptatum est sit reprehenderit! Nesciunt ducimus vitae perferendis numquam, nam, velit minus! Dolores, fuga. Lorem ipsum dolor sit amet Debitis facere minima maiores necessitatibus! Et, distinctio dolores tempore magnam.', NULL),
(42, 9, 27, '2017-11-29 13:53:26', 'Vel inventore, eveniet odio! Maiores, iste minima quidem aperiam! Lorem ipsum dolor sit. Lorem ipsum dolor sit amet Illo tenetur id nam sunt recusandae sed voluptas, nostrum ullam. Ratione magni nobis laboriosam esse cumque. Adipisci praesentium nostrum est. Et, distinctio dolores tempore magnam. Perspiciatis enim nisi ut!', NULL),
(43, 10, 27, '2017-11-29 13:53:27', 'Odio veritatis, hic nihil architecto! Vel inventore, eveniet odio! Amet aliquam, quam voluptas libero aliquid est molestias natus ex! Perspiciatis enim nisi ut!', NULL),
(44, NULL, 27, '2017-11-29 13:53:28', 'Perspiciatis enim nisi ut! Maiores, iste minima quidem aperiam! Animi quasi reprehenderit, sapiente adipisci? Illo tenetur id nam sunt recusandae sed voluptas, nostrum ullam. Lorem ipsum dolor sit. Voluptatum est sit reprehenderit! Amet aliquam, quam voluptas libero aliquid est molestias natus ex! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, reprehenderit! Et, distinctio dolores tempore magnam.', NULL),
(45, 10, 27, '2017-11-29 13:53:29', 'Nesciunt ducimus vitae perferendis numquam, nam, velit minus! Dolores, fuga. Maiores, iste minima quidem aperiam! Ratione magni nobis laboriosam esse cumque. Adipisci praesentium nostrum est. Voluptatum est sit reprehenderit! Vel inventore, eveniet odio! Sunt expedita minus a. Et, distinctio dolores tempore magnam. Animi quasi reprehenderit, sapiente adipisci?', NULL),
(46, 10, 27, '2017-11-29 13:53:30', 'Debitis facere minima maiores necessitatibus! Sunt expedita minus a. Voluptatum est sit reprehenderit! Vel inventore, eveniet odio!', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `ob_contact`
--

CREATE TABLE IF NOT EXISTS `ob_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_id_user` int(11) DEFAULT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `sendAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_ECFB4685F69CF767` (`fk_id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Structure de la table `ob_photo`
--

CREATE TABLE IF NOT EXISTS `ob_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_post_id` int(11) DEFAULT NULL,
  `title` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_5F8DF995F47645AE` (`url`),
  UNIQUE KEY `UNIQ_5F8DF995BBA63E00` (`fk_post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

--
-- Contenu de la table `ob_photo`
--

INSERT INTO `ob_photo` (`id`, `fk_post_id`, `title`, `url`) VALUES
(15, 15, 'Musée d''histoire nat', 'uploads/sample0.jpg'),
(16, 16, 'Opéra Garnier', 'uploads/sample1.jpg'),
(17, 17, 'Canal-Saint-Martin', 'uploads/sample2.jpg'),
(18, 18, 'Keith Haring', 'uploads/sample3.jpg'),
(19, 19, 'Notre-Dame de Paris', 'uploads/sample4.jpg'),
(20, 20, 'Tour Eiffel', 'uploads/sample5.jpg'),
(21, 21, 'Sacré Coeur de Montm', 'uploads/sample6.jpg'),
(22, 22, 'Ecluse', 'uploads/sample7.jpg'),
(23, 23, 'Musée du Louvre', 'uploads/sample8.jpg'),
(24, 24, 'Carte', 'uploads/sample9.jpg'),
(25, 25, 'Place de la Républiq', 'uploads/sample10.jpg'),
(26, 26, 'Eglise de la Madelei', 'uploads/sample11.jpg'),
(27, 27, 'Hôtel de Ville de Pa', 'uploads/sample12.jpg'),
(28, 28, 'Place de la Concorde', 'uploads/sample13.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `ob_post`
--

CREATE TABLE IF NOT EXISTS `ob_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_user_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `published_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_6423A6A8989D9B62` (`slug`),
  KEY `IDX_6423A6A85741EEB9` (`fk_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

--
-- Contenu de la table `ob_post`
--

INSERT INTO `ob_post` (`id`, `fk_user_id`, `created_at`, `updated_at`, `title`, `slug`, `content`, `is_published`, `published_at`) VALUES
(15, 10, '2017-11-29 13:53:25', NULL, 'Illo tenetur id nam sunt recusandae sed voluptas, nostrum ullam.', NULL, 'Fugit quasi iste minima cumque laborum est laudantium provident ad perspiciatis culpa consequuntur ratione nobis vero illum reprehenderit, quos labore suscipit excepturi illo, nam quibusdam ducimus rerum. Amet impedit veritatis quod iure nobis velit aspernatur! Possimus, libero voluptatem! Obcaecati veniam iste minima esse non dignissimos sequi alias quae, itaque voluptatum totam quia eius dolorem voluptatibus ipsum quibusdam quas, suscipit! Nulla, animi suscipit ad eveniet voluptates, modi laudantium quisquam necessitatibus alias odio nemo reprehenderit debitis, ipsa reiciendis. Ipsa, ipsam tempora maiores, animi esse et perspiciatis excepturi praesentium corporis placeat aliquam blanditiis iste adipisci dignissimos. Ducimus rem sequi ea, soluta assumenda quo.', 0, NULL),
(16, 10, '2017-11-28 13:53:25', NULL, 'Perspiciatis enim nisi ut!', NULL, 'Ipsum laudantium laboriosam saepe. Quaerat nisi perspiciatis cumque beatae vitae quod qui dignissimos iure distinctio aliquid, at voluptates sint dolor, eius quis nesciunt facilis excepturi recusandae, repellat inventore deserunt ipsam quisquam? Tempore nisi tenetur, voluptas atque. Neque, molestias doloribus non magni libero et soluta assumenda voluptates perspiciatis vitae, omnis beatae aspernatur, obcaecati eos autem nam nulla voluptatem. Esse asperiores temporibus distinctio, quidem debitis cumque ipsum mollitia, eaque! Est sunt voluptas, vel rerum animi odit praesentium unde earum tenetur itaque similique soluta saepe recusandae necessitatibus ipsam reprehenderit, facilis eligendi. Adipisci optio rerum eaque explicabo maiores minus, soluta iusto blanditiis dolorem obcaecati!', 1, '2017-11-28 13:53:25'),
(17, 9, '2017-11-27 13:53:25', NULL, 'Ratione magni nobis laboriosam esse cumque. Adipisci praesentium nostrum est.', NULL, 'Ipsum laudantium laboriosam saepe. Quaerat nisi perspiciatis cumque beatae vitae quod qui dignissimos iure distinctio aliquid, at voluptates sint dolor, eius quis nesciunt facilis excepturi recusandae, repellat inventore deserunt ipsam quisquam? Tempore nisi tenetur, voluptas atque. Neque, molestias doloribus non magni libero et soluta assumenda voluptates perspiciatis vitae, omnis beatae aspernatur, obcaecati eos autem nam nulla voluptatem. Esse asperiores temporibus distinctio, quidem debitis cumque ipsum mollitia, eaque! Est sunt voluptas, vel rerum animi odit praesentium unde earum tenetur itaque similique soluta saepe recusandae necessitatibus ipsam reprehenderit, facilis eligendi. Adipisci optio rerum eaque explicabo maiores minus, soluta iusto blanditiis dolorem obcaecati!', 0, NULL),
(18, 8, '2017-11-26 13:53:25', NULL, 'Lorem ipsum dolor sit amet', NULL, 'Laudantium, saepe, similique? Labore, consequatur, doloribus! Quis labore totam, adipisci aliquam repudiandae quisquam. Neque praesentium qui nesciunt consectetur consequatur dolor suscipit minus ullam molestiae quisquam optio, fuga explicabo exercitationem numquam incidunt. Sunt aliquid officia blanditiis aut eligendi consequuntur, asperiores, laudantium quisquam dolor laborum atque. Labore libero placeat magnam accusamus eligendi esse tenetur voluptatem ratione eius. Inventore dolores, nemo illum cum libero numquam eaque enim, tempore fuga in consequatur repellendus quas iusto consequuntur corporis soluta. Facere doloremque ex tempore nesciunt assumenda, molestiae, voluptatibus voluptates excepturi quae amet natus, deleniti illum esse dolor voluptas similique est nam nostrum, provident praesentium iure. Nemo?', 0, NULL),
(19, 8, '2017-11-25 13:53:25', NULL, 'Odio veritatis, hic nihil architecto!', NULL, 'Fugit quasi iste minima cumque laborum est laudantium provident ad perspiciatis culpa consequuntur ratione nobis vero illum reprehenderit, quos labore suscipit excepturi illo, nam quibusdam ducimus rerum. Amet impedit veritatis quod iure nobis velit aspernatur! Possimus, libero voluptatem! Obcaecati veniam iste minima esse non dignissimos sequi alias quae, itaque voluptatum totam quia eius dolorem voluptatibus ipsum quibusdam quas, suscipit! Nulla, animi suscipit ad eveniet voluptates, modi laudantium quisquam necessitatibus alias odio nemo reprehenderit debitis, ipsa reiciendis. Ipsa, ipsam tempora maiores, animi esse et perspiciatis excepturi praesentium corporis placeat aliquam blanditiis iste adipisci dignissimos. Ducimus rem sequi ea, soluta assumenda quo.', 1, '2017-11-25 13:53:25'),
(20, 10, '2017-11-24 13:53:25', NULL, 'Amet aliquam, quam voluptas libero aliquid est molestias natus ex!', NULL, 'Ipsum laudantium laboriosam saepe. Quaerat nisi perspiciatis cumque beatae vitae quod qui dignissimos iure distinctio aliquid, at voluptates sint dolor, eius quis nesciunt facilis excepturi recusandae, repellat inventore deserunt ipsam quisquam? Tempore nisi tenetur, voluptas atque. Neque, molestias doloribus non magni libero et soluta assumenda voluptates perspiciatis vitae, omnis beatae aspernatur, obcaecati eos autem nam nulla voluptatem. Esse asperiores temporibus distinctio, quidem debitis cumque ipsum mollitia, eaque! Est sunt voluptas, vel rerum animi odit praesentium unde earum tenetur itaque similique soluta saepe recusandae necessitatibus ipsam reprehenderit, facilis eligendi. Adipisci optio rerum eaque explicabo maiores minus, soluta iusto blanditiis dolorem obcaecati!', 0, NULL),
(21, 8, '2017-11-23 13:53:25', NULL, 'Voluptatum est sit reprehenderit!', NULL, 'Fugit quasi iste minima cumque laborum est laudantium provident ad perspiciatis culpa consequuntur ratione nobis vero illum reprehenderit, quos labore suscipit excepturi illo, nam quibusdam ducimus rerum. Amet impedit veritatis quod iure nobis velit aspernatur! Possimus, libero voluptatem! Obcaecati veniam iste minima esse non dignissimos sequi alias quae, itaque voluptatum totam quia eius dolorem voluptatibus ipsum quibusdam quas, suscipit! Nulla, animi suscipit ad eveniet voluptates, modi laudantium quisquam necessitatibus alias odio nemo reprehenderit debitis, ipsa reiciendis. Ipsa, ipsam tempora maiores, animi esse et perspiciatis excepturi praesentium corporis placeat aliquam blanditiis iste adipisci dignissimos. Ducimus rem sequi ea, soluta assumenda quo.', 1, '2017-11-23 13:53:25'),
(22, 9, '2017-11-22 13:53:25', NULL, 'Aperiam saepe officia dignissimos neque animi voluptatibus, laudantium suscipit, quos.', NULL, 'Fugit quasi iste minima cumque laborum est laudantium provident ad perspiciatis culpa consequuntur ratione nobis vero illum reprehenderit, quos labore suscipit excepturi illo, nam quibusdam ducimus rerum. Amet impedit veritatis quod iure nobis velit aspernatur! Possimus, libero voluptatem! Obcaecati veniam iste minima esse non dignissimos sequi alias quae, itaque voluptatum totam quia eius dolorem voluptatibus ipsum quibusdam quas, suscipit! Nulla, animi suscipit ad eveniet voluptates, modi laudantium quisquam necessitatibus alias odio nemo reprehenderit debitis, ipsa reiciendis. Ipsa, ipsam tempora maiores, animi esse et perspiciatis excepturi praesentium corporis placeat aliquam blanditiis iste adipisci dignissimos. Ducimus rem sequi ea, soluta assumenda quo.', 1, '2017-11-22 13:53:25'),
(23, 9, '2017-11-21 13:53:25', NULL, 'Maiores, iste minima quidem aperiam!', NULL, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta quod maiores a culpa ab doloribus neque quo quos molestiae magnam repudiandae recusandae repellendus voluptatum vel quas temporibus alias nobis, corporis nesciunt velit fugit! Magnam ea, cumque deleniti? Aliquid maiores est quas similique ad! Commodi illum sint in ratione ipsa necessitatibus accusantium maiores amet quisquam quae hic neque nihil nulla quaerat sequi odio nemo at labore, dolor aut et saepe voluptatem numquam sit. Debitis corrupti itaque, odit qui quae adipisci at ex quibusdam facere maiores culpa ut et vitae! Reiciendis temporibus nihil, doloremque odio natus? Doloribus earum repellendus illum neque eaque?', 1, '2017-11-21 13:53:25'),
(24, 10, '2017-11-20 13:53:25', NULL, 'Et, distinctio dolores tempore magnam.', NULL, 'Fugit quasi iste minima cumque laborum est laudantium provident ad perspiciatis culpa consequuntur ratione nobis vero illum reprehenderit, quos labore suscipit excepturi illo, nam quibusdam ducimus rerum. Amet impedit veritatis quod iure nobis velit aspernatur! Possimus, libero voluptatem! Obcaecati veniam iste minima esse non dignissimos sequi alias quae, itaque voluptatum totam quia eius dolorem voluptatibus ipsum quibusdam quas, suscipit! Nulla, animi suscipit ad eveniet voluptates, modi laudantium quisquam necessitatibus alias odio nemo reprehenderit debitis, ipsa reiciendis. Ipsa, ipsam tempora maiores, animi esse et perspiciatis excepturi praesentium corporis placeat aliquam blanditiis iste adipisci dignissimos. Ducimus rem sequi ea, soluta assumenda quo.', 1, '2017-11-20 13:53:25'),
(25, 8, '2017-11-19 13:53:25', NULL, 'Lorem ipsum dolor sit.', NULL, 'Laudantium, saepe, similique? Labore, consequatur, doloribus! Quis labore totam, adipisci aliquam repudiandae quisquam. Neque praesentium qui nesciunt consectetur consequatur dolor suscipit minus ullam molestiae quisquam optio, fuga explicabo exercitationem numquam incidunt. Sunt aliquid officia blanditiis aut eligendi consequuntur, asperiores, laudantium quisquam dolor laborum atque. Labore libero placeat magnam accusamus eligendi esse tenetur voluptatem ratione eius. Inventore dolores, nemo illum cum libero numquam eaque enim, tempore fuga in consequatur repellendus quas iusto consequuntur corporis soluta. Facere doloremque ex tempore nesciunt assumenda, molestiae, voluptatibus voluptates excepturi quae amet natus, deleniti illum esse dolor voluptas similique est nam nostrum, provident praesentium iure. Nemo?', 1, '2017-11-19 13:53:25'),
(26, 10, '2017-11-18 13:53:25', NULL, 'Debitis facere minima maiores necessitatibus!', NULL, 'Ipsum laudantium laboriosam saepe. Quaerat nisi perspiciatis cumque beatae vitae quod qui dignissimos iure distinctio aliquid, at voluptates sint dolor, eius quis nesciunt facilis excepturi recusandae, repellat inventore deserunt ipsam quisquam? Tempore nisi tenetur, voluptas atque. Neque, molestias doloribus non magni libero et soluta assumenda voluptates perspiciatis vitae, omnis beatae aspernatur, obcaecati eos autem nam nulla voluptatem. Esse asperiores temporibus distinctio, quidem debitis cumque ipsum mollitia, eaque! Est sunt voluptas, vel rerum animi odit praesentium unde earum tenetur itaque similique soluta saepe recusandae necessitatibus ipsam reprehenderit, facilis eligendi. Adipisci optio rerum eaque explicabo maiores minus, soluta iusto blanditiis dolorem obcaecati!', 1, '2017-11-18 13:53:25'),
(27, 9, '2017-11-17 13:53:25', NULL, 'Nesciunt ducimus vitae perferendis numquam, nam, velit minus! Dolores, fuga.', NULL, 'Ipsum laudantium laboriosam saepe. Quaerat nisi perspiciatis cumque beatae vitae quod qui dignissimos iure distinctio aliquid, at voluptates sint dolor, eius quis nesciunt facilis excepturi recusandae, repellat inventore deserunt ipsam quisquam? Tempore nisi tenetur, voluptas atque. Neque, molestias doloribus non magni libero et soluta assumenda voluptates perspiciatis vitae, omnis beatae aspernatur, obcaecati eos autem nam nulla voluptatem. Esse asperiores temporibus distinctio, quidem debitis cumque ipsum mollitia, eaque! Est sunt voluptas, vel rerum animi odit praesentium unde earum tenetur itaque similique soluta saepe recusandae necessitatibus ipsam reprehenderit, facilis eligendi. Adipisci optio rerum eaque explicabo maiores minus, soluta iusto blanditiis dolorem obcaecati!', 1, '2017-11-17 13:53:25'),
(28, 9, '2017-11-16 13:53:25', NULL, 'Animi quasi reprehenderit, sapiente adipisci?', NULL, 'Assumenda alias totam, temporibus reprehenderit beatae sint neque consequuntur harum aliquid eaque laudantium eum ratione, distinctio asperiores quam iure atque cupiditate ipsum magnam, iusto ducimus molestias quisquam sapiente itaque incidunt. Atque rerum maiores aut pariatur quas tempore voluptate, amet voluptatum a quam, dignissimos nesciunt necessitatibus aperiam vitae! Facere dolor mollitia, excepturi sequi asperiores pariatur sed consectetur officiis odit totam dolore alias velit, accusantium atque impedit, incidunt reprehenderit provident architecto at vitae. Amet aliquid tenetur sunt eum, pariatur error quae nostrum delectus consequatur dignissimos iste architecto qui, ducimus voluptates aliquam itaque laudantium blanditiis? Cum quos deleniti dignissimos perspiciatis quasi vel officia.', 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `ob_post_category`
--

CREATE TABLE IF NOT EXISTS `ob_post_category` (
  `post_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`post_id`,`category_id`),
  KEY `IDX_76E2FC134B89032C` (`post_id`),
  KEY `IDX_76E2FC1312469DE2` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `ob_post_category`
--

INSERT INTO `ob_post_category` (`post_id`, `category_id`) VALUES
(15, 14),
(15, 15),
(15, 16),
(16, 13),
(16, 19),
(16, 21),
(16, 22),
(17, 16),
(17, 17),
(17, 24),
(18, 23),
(19, 19),
(19, 22),
(20, 13),
(20, 14),
(20, 24),
(21, 13),
(21, 17),
(21, 19),
(22, 14),
(22, 18),
(22, 20),
(22, 21),
(23, 13),
(24, 15),
(24, 19),
(24, 21),
(24, 24),
(25, 14),
(25, 16),
(25, 19),
(25, 20),
(26, 13),
(26, 17),
(26, 19),
(27, 13),
(27, 17),
(27, 20),
(28, 13),
(28, 17),
(28, 22);

-- --------------------------------------------------------

--
-- Structure de la table `ob_user`
--

CREATE TABLE IF NOT EXISTS `ob_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_avatar_id` int(11) DEFAULT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `is_active` tinyint(1) NOT NULL,
  `registrated_at` datetime NOT NULL,
  `registrationKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B33A1C6CF85E0677` (`username`),
  UNIQUE KEY `UNIQ_B33A1C6CE7927C74` (`email`),
  KEY `IDX_B33A1C6CE761EC80` (`fk_avatar_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Contenu de la table `ob_user`
--

INSERT INTO `ob_user` (`id`, `fk_avatar_id`, `username`, `firstname`, `lastname`, `email`, `password`, `roles`, `is_active`, `registrated_at`, `registrationKey`) VALUES
(8, 16, 'Toto', 'FToto', 'LToto', 'toto@gmail.com', '$2y$12$f.Y9B5Pfwc8DD7M9Lu85xO8ZGJhdIG69IwGz5Yvvn1mGBq3lqZ.V2', 'a:1:{i:0;s:9:"ROLE_USER";}', 1, '2017-11-29 13:53:24', '64a00ee1ea549586ba2faaa696caf467'),
(9, 16, 'Tata', 'FTata', 'LTata', 'tata@gmail.com', '$2y$12$4QUkEaQtU.TVBxuXgG1CN.NbWqjCWkmju6k8Y/PBFsZ8wG7uxFUh.', 'a:1:{i:0;s:9:"ROLE_USER";}', 1, '2017-11-29 13:53:24', '7e9fe8ee0880fd286701e9b6339708f4'),
(10, 18, 'Titi', 'FTiti', 'LTiti', 'titi@gmail.com', '$2y$12$VJCXhJ5gz/TNybTuF6V43uHsg7kB4YPvdwXaRtNfMNw6PEBSQgg5K', 'a:1:{i:0;s:9:"ROLE_USER";}', 1, '2017-11-29 13:53:25', 'ff50045ea0d7fb5461a6199021d08a22'),
(11, 13, 'admin', 'Fadmin', 'Ladmin', 'admin@gmail.com', '$2y$12$U/KSRbD1a4c3kJADgW77AeTxeMUHrVQRQHvlDTX8uZITYvFx/DY7i', 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 1, '2017-11-29 13:53:25', '43a97f882522b1ea93e8c1e1f07b650e');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `ob_comment`
--
ALTER TABLE `ob_comment`
  ADD CONSTRAINT `FK_34EDF2D1BBA63E00` FOREIGN KEY (`fk_post_id`) REFERENCES `ob_post` (`id`),
  ADD CONSTRAINT `FK_34EDF2D15741EEB9` FOREIGN KEY (`fk_user_id`) REFERENCES `ob_user` (`id`);

--
-- Contraintes pour la table `ob_contact`
--
ALTER TABLE `ob_contact`
  ADD CONSTRAINT `FK_ECFB4685F69CF767` FOREIGN KEY (`fk_id_user`) REFERENCES `ob_user` (`id`);

--
-- Contraintes pour la table `ob_photo`
--
ALTER TABLE `ob_photo`
  ADD CONSTRAINT `FK_5F8DF995BBA63E00` FOREIGN KEY (`fk_post_id`) REFERENCES `ob_post` (`id`);

--
-- Contraintes pour la table `ob_post`
--
ALTER TABLE `ob_post`
  ADD CONSTRAINT `FK_6423A6A85741EEB9` FOREIGN KEY (`fk_user_id`) REFERENCES `ob_user` (`id`);

--
-- Contraintes pour la table `ob_post_category`
--
ALTER TABLE `ob_post_category`
  ADD CONSTRAINT `FK_76E2FC1312469DE2` FOREIGN KEY (`category_id`) REFERENCES `ob_category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_76E2FC134B89032C` FOREIGN KEY (`post_id`) REFERENCES `ob_post` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `ob_user`
--
ALTER TABLE `ob_user`
  ADD CONSTRAINT `FK_B33A1C6CE761EC80` FOREIGN KEY (`fk_avatar_id`) REFERENCES `ob_avatar` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
